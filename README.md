# projectadil
****Exercice installation git****

![](images/1.PNG)

![](images/2.PNG)

****Exercice Dépot local****

*Création d'un nouveau dépot/ajouter fichiers/modifications/commits

![](images/4.PNG)

![](images/5.PNG)

![](images/6.PNG)

![](images/7.PNG)

*Renommer et supprimer les fichiers

![](images/8.PNG)

![](images/9.PNG)

*historique des commits

![](images/10.PNG)

![](images/11.PNG)

*ajouter une étiquette à un ancien commit

![](images/12.PNG)

****Exercice dépot distant****

**Exercice 1**

*création d'un dépot sur gitlab

![](images/13.PNG)

![](images/14.PNG)

*ajouter/commiter/pusher

![](images/15.PNG)

![](images/16.PNG)

**Exercice 2**

Créer un dépot sur le serveur gitlab et ajouter commiter et pusher des fichiers

![](Nouveau/34.PNG)

Ajouter un collègue (fictifAD)

![](Nouveau/33.PNG)

vérifier la récupération des modification faites par un collègue

![](Nouveau/32.PNG)

conflit lors des modifications en parallèle

![](image2/8.PNG)

****Exercice Branche****

cloner un dépot distant

![](Nouveau/22.PNG)

Créer unr brache b1, faire des commits dans b1 et fusionner les dans le main

![](Nouveau/23.PNG)

![](Nouveau/24.PNG)

envoyer b1 sur le serveur et vérification

![](Nouveau/25.PNG)

![](Nouveau/26.PNG)

![](Nouveau/27.PNG)

branche b2 commits et fusions des commits dans le main

![](Nouveau/28.PNG)

![](Nouveau/29.PNG)

fusionner main dans b1

![](Nouveau/35.PNG)

![](Nouveau/36.PNG)

vérification du graphe des commits

![](image2/9.PNG)

![](image2/10.PNG)

envoyer b2 sur le serveur et suppréssion sur le dépot local

![](Nouveau/37.PNG)

suppréssion de b2 sur le dépot distant

![](Nouveau/38.PNG)

---->

![](Nouveau/39.PNG)

****Exercice FORK****

**Exercice 2**

créer deux dépots distant public (testdossierpub) et l'autre privé (testdossierpriv)

![](image2/1.PNG)

cloner dépot privé (testdossierpriv) faire des commits et pusher

![](image2/2.PNG)

créer une branche "public", fusionner-y le master

![](image2/3.PNG)

pusher "public" sue le dépot distant privé (testdossierpriv) et sur le dépot distant public (testdossierpub)

![](image2/4.PNG)

![](image2/5.PNG)

![](image2/6.PNG)

faire un commit dans le main, fusionner le dans la branche "public" et pusher

![](image2/7.PNG)

cloner le dépot public et le mettre à jour avec la branche "public" du dépot distant privé

